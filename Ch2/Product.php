<?php
abstract class Ch2_Product
{
    protected $_type;
    protected $_title;
    protected $_manufacturer;

    public function __construct()
    {
        $this->_manufacturer = new Ch2_Manufacturer();
    }

    public function getProductType()
    {
        return $this->_type;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function setTitle($title)
    {
        $this->_title = $title;
    }

    public function setManufacturerName($name)
    {
        $this->_manufacturer->setManufacturerName($name);
    }

    public function getManufacturerName()
    {
        return $this->_manufacturer->getManufacturerName();
    }

    public function __clone()
    {
        $this->_manufacturer = clone $this->_manufacturer;
    }

    abstract public function display();
}
